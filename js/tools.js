function makeFrames(channel, winSize, overlap, winType = 'rect') {

    if (overlap >= winSize) {
        alert("Overlap must be smaller than window size!");
        return -1;
    }
    
    // copy data into new array to allow zero padding later
    let buffer = [];

    for (let i = 0; i < channel.length; ++i) {
        buffer.push(channel[i]);
    }
    if (channel.length < winSize) {
        for (let i = 0; i < winSize - buffer.length; ++i){
            buffer.push(0);
        }
    }

    
    // create window
    let win = [];

    switch(winType) {
        case 'rect':
            for (let i = 0; i < winSize; ++i) {
                win.push(1);
            }
        break;
        case 'hamm':
            for (let i = 0; i < winSize; ++i) {
                win.push(0.54 - 0.45 * Math.cos(2 * Math.PI * i /(winSize - 1)));
            }
        break;
        case 'black':
            let a0 = 0.42;
            let a1 = 0.5;
            let a2 = 0.08;
            for (let i = 0; i < winSize; ++i) {
                win.push(a0 - a1 * Math.cos(2 * Math.PI * i /(winSize - 1)) + a2 * Math.cos(4 * Math.PI * i / (winSize - 1)));
            }
    }

    let zeroPadd = (winSize - overlap) - (buffer.length - winSize) % (winSize - overlap);

    for (let z = 0; z < zeroPadd; ++z) {
        buffer.push(0);
    }

    let matrix = []
    //let numFrames = Math.floor((buffer.length - winSize) / (winSize - overlap)) + 1;
    let numFrames = (buffer.length - winSize) / (winSize - overlap) + 1;

    //make matrix
    for (let f = 0; f < numFrames; ++f) {

        let currFrame = [];

        for (let s = 0; s < winSize; ++s) {
            currFrame.push(buffer[f * (winSize - overlap) + s] * win[s]);
        }

        matrix.push(currFrame);
    }

    return matrix;
}

function getFreqIndex( winLength, fs, f ) {
    return Math.round(f * winLength / fs);
}