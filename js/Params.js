/* define(['pitchfinder', 'tools'], function(Pitchfinder, tools) { */

class Param {

    constructor({ name , numb_values , calculate , link = '' , explanation = '', active = true,  selected = false }){

        this.active = active;
        if (this.active) {
            this.name = name;
        } else {
            this.name = name + ' [COMING SOON]';
        }
        
        this.numb_values = numb_values;
        this.selected = selected;
        this.link = link;
        this.explanation = explanation;
        this.calculate = calculate || function(){ return 0 };
    }
}

function getParams() {

    let params = [];

    params.push(new Param({
        
        name:'Mean RMS', 
        numb_values: 1, 
        link: 'https://en.wikipedia.org/wiki/Root_mean_square#Definition',
        calculate: function({ audio , fs }) {

            let totalSum = 0.0;

            for(let c = 0; c < audio.length; ++c)
            {
                let channel_sum = 0.0;
                
                for(let i = 0; i < audio[c].length; ++i)
                {
                    channel_sum += Math.pow(audio[c][i], 2);
                }
    
                channel_sum = Math.sqrt(channel_sum / audio.length);
                totalSum += Math.pow(channel_sum, 2);
            }
    
            return Math.sqrt(totalSum);
        } 
    }));

    params.push(new Param({
        
        name: "Zero Crossings Rate", 
        numb_values: 1,
        link: "https://en.wikipedia.org/wiki/Zero-crossing_rate",
        calculate: function({ audio , fs }){
        
            let zc = 0.0;

            for(let c = 0; c < audio.length; ++c)
            {
                for(i = 1; i < audio[c].length; ++i)
                {
                    if(Math.abs(Math.sign(audio[c][i]) - Math.sign(audio[c][i-1])) > 1) 
                    {
                        zc += 1;
                    }
                }
            }
            
            zc = zc / audio.length;
            return zc / (audio[0].length/fs);

        }
    }));
    params.push(new Param({
        name: 'Hammarberg Index',
        numb_values: 1,
        link: 'https://books.google.fr/books?id=AFBECwAAQBAJ&pg=PA38&lpg=PA38&dq=hammarberg+et+al+1980&source=bl&ots=OiZWenPqdw&sig=PI-So97dahPH5FINMzRm7jofa30&hl=de&sa=X&ved=0ahUKEwigoO6trMnZAhVFOxQKHTbKA6AQ6AEIPDAC#v=onepage&q=hammarberg%20et%20al%201980&f=false',
        calculate: function({ freq_abs , fs }){
            
            let hammarberg = 0;

            for (let c = 0; c < freq_abs.length; ++c) {
                let split1 = getFreqIndex(freq_abs[c].length, fs, 2000);
                let split2 = getFreqIndex(freq_abs[c].length, fs, 5000) - split1;
                /* let nquist = getFreqIndex(freq_abs[c].length, fs, fs/2); */

                let part1 = freq_abs[c].splice(0, split1 + 1);
                let part2 = freq_abs[c].splice(0, split2 + 1);

                let max1 = Math.max(...part1);
                let max2 = Math.max(...part2);

                hammarberg += max1/max2;
            }
            
            return hammarberg / freq_abs.length;
        }
    }))
    params.push(new Param({
        
        name: "Pitch (Yin)",
        numb_values: 1,
        active: false,
        link: "https://github.com/peterkhayes/pitchfinder/blob/master/src/detectors/yin.js",
        calculate: function({ audio , fs }) {
        
            let YINDetector = PitchFinder.YIN({
                    samplerate: fs,
                    bufferSize: 2048
            });

            let pitches = [];

            for(let c = 0; c < audio.length; ++c) {

                let frames = makeFrames(audio[c], 2048, 1024, 'hamm');

                for(let f = 0; f < frames.length; ++f){
                    
                    let estimate = YINDetector(frames[f]);

                    if(estimate != null && estimate > 0) {
                        console.log(estimate.freq);
                        pitches.push(estimate);
                    }
                }
            }

            let finPitch = 0;

            for (let p = 0; p < pitches.length; ++p) {
                finPitch += pitches[p];
            }

            if (pitches.length > 0) {
                finPitch /= pitches.length;
            }

            return finPitch;
        }
    })); 

    return params;
}
/* 
}); */
