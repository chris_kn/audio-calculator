requirejs.config({
    baseUrl: 'scripts',
    
    paths: {
        jquery: [
            'http://code.jquery.com/jquery-3.3.1.min.js',
            'jquery-3.3.1.min',
                    
        ],
        xlsx: 'extScripts/xlsx.core.min',
        filesaver: 'extScripts/FileSaver.min',
        tableexport: 'extScripts/tableexport.min',
        pitchfinder: 'extScripts/pitchFinder',
        amdfPitch: 'extScripts/detectors/amdf',
        yinPitch: 'extScripts/detectors/yin',
        waveletPitch: 'extScripts/detectors/dynamic_wavelet',
        macleodPitch: 'extScripts/detectors/macleod',
        freqScript: 'extScripts/tools/frequencies',
        Params: 'Params',
        main: 'main',
        tools: 'tools'
    }
});