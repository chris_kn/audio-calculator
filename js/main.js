'use strict';

// Things to do at site loading
var isAdvancedUpload = function() {
    var div = document.createElement('div');
    return (('draggable' in div) || ('ondragstart' in div && 'ondrop' in div)) && 'FormData' in window && 'FileReader' in window;
}();

let params;
let acceptedFormats = ['wav', 'mp3', 'ogg', 'flac'];
let selectedParams = [];
let resultsTable;

$(document).ready(function() {
    $('#fullpage').fullpage({
    	fixedElements: '#contact-space, #impressum-space',
        scrollingSpeed: 1000,
        /* scrollOverflow: true, */
        /* navigation: true, */
        /* navigationPosition: 'left', */
        
    });
    $('.hidden').css('display', 'none');
    params = getParams();
    captchaCode();
    drawParamList();
    drawSelectedList();
    $('#resultsSpace').tableExport();
});

$('#dropzone').on('dragover', function() {
    $(this).addClass('hover');
});
  
$('#dropzone').on('dragleave', function() {
    $(this).removeClass('hover');
});
  
$('#dropzone input').on('change', function(e) {

    //let files = this.files;
    let uncheckedFiles = Array.prototype.slice.call(e.target.files);
    let files = [];
    $('#dropzone').removeClass('hover');
    $('#scroll-after-calculation').removeClass('scroll-indication');

    for (var f = 0; f < uncheckedFiles.length; ++f)
    {
        let ext = uncheckedFiles[f].name.split('.').pop();
        if ( acceptedFormats.indexOf(ext) > -1) {
            files.push(uncheckedFiles[f]);
        }
    }

    if(files.length == 0)
    {
        alert('File type not allowed. Only the typical audio files can be read.');
        return 
    }

    if(files.length < uncheckedFiles.length) {
        
        const text = (uncheckedFiles.length - files.length) + " files can't be read. Do you want to proceed with the remaining " + files.length + " files?";
        
        if(!window.confirm(text)) {
            return
        }
    }

    $('#dropzone').addClass('dropped');
    
    writeList(files);

    resultsArray = new Array(files.length);
    resultsArray[0] = '<table>';
    resultsArray[1] = '<tr><th>Filename</th>';

    for (let p = 0; p < params.length; ++p) {
        if (params[p].selected) {
            resultsArray[1] += '<th>' + params[p].name + '</th>'; 
        }
    }
    
    resultsArray[1] += '</tr>';

    count = 0;

    /* files.forEach(function(file, f){ */
    for (let f = 0; f < files.length; ++f){

        let currentResults = [];
        let reader = new FileReader();

        reader.onload = function() {

            //let context = new AudioContext();
            let context = new (window.AudioContext || window.webkitAudioContext)();

            context.decodeAudioData(reader.result, function(decodedBuffer){
            
                channels = [];
                
                for (let c = 0; c < decodedBuffer.numberOfChannels; ++c) {
                    channels[c] = new Float32Array(decodedBuffer.length);
                    channels[c] = decodedBuffer.getChannelData(c);
                }
                
                currentResults = extractParams(channels, decodedBuffer.sampleRate);
                
                resultsArray[f + 2] = '<tr><td>' + files[f].name + '</td>';

                for (let p = 0; p < currentResults.length; ++p) {
                    resultsArray[f + 2] += '<td>' + currentResults[p] + '</td>';
                }

                resultsArray[f + 2] += '</tr>';

                ++count;
                
                let progressNum = 100 - (count / files.length) * 100;
                let progress = 'inset(0 0 ' + progressNum + '% 0)';

                $('#progress-disp').css('clip-path', progress);

                if (count == files.length) {
                    
                    // write results from table array in one string
                    resultsArray[files.length + 2] = '</table>';
                    resultsTable = '';

                    for(let r = 0; r < resultsArray.length; ++r) {
                        resultsTable += resultsArray[r];
                    }
                    
                    $('#resultsSpace').html(resultsTable);
                    $('#resultsSpace').tableExport();
                    $('#progress-disp').css('clip-path', 'inset(0 0 100% 0)');
                    $('#export-space').css('height', '100%');
                    $('#scroll-after-calculation').addClass('scroll-indication');
                    /* $('#scroll-after-calculation').css('display', 'block'); */
                }
            });
        
        };

        reader.onerror = function(event) {
            console.error("File could not be read! Code " + event.target.error.code);
        };

        reader.readAsArrayBuffer(files[f]);
    };

});

function writeList(files) {

    // files is a FileList of File objects. List some properties.
    var output = [];
    for (var i = 0, f; f = files[i]; i++) {
      output.push('<li><strong>', escape(f.name), '</strong> (', f.type || 'n/a', ') - ',
                  f.size, ' bytes, last modified: ',
                  f.lastModifiedDate ? f.lastModifiedDate.toLocaleDateString() : 'n/a',
                  '</li>');
    }
    $('#dropinfo').hide();
    //$('#list').css('display', 'block');
    $('#list').html('<ul>' + output.join('') + '</ul>');
    
}

function extractParams(channels, fs) {
    
    let results = [];

    // perform FFT
    freqR = [];
    freqI = [];
    magnitude = [];

    for (let c = 0 ; c < channels.length; ++c) {
        freqR[c] = channels[c].slice(0);
        freqI[c] = new Float32Array(freqR[c].length).fill(0.0);
        transform(freqR[c], freqI[c]);
        magnitude[c] = [];
        for (let s = 0; s < freqR[c].length; ++s) {
            magnitude[c][s] = Math.sqrt(Math.pow(freqR[c][s], 2) + Math.pow(freqI[c][s], 2));
        }
    }

    let data = {
        audio: channels,
        freq_abs: magnitude,
        freq_r: freqR,
        freq_i: freqI,
        fs: fs
    }

    for (let p = 0; p < params.length; ++p)
    {
        if (params[p].selected == false) {
            continue;
        }

        results[results.length] = params[p].calculate(data);
        if (results.length > 0) {
            console.log("" + results[results.length - 1]);
        }
    }

    return results;
}

function drawParamList() {

    let pBox = $('#params-box');
    pBox.html('<div>All Parameters</div>');

    for(let p = 0; p < params.length; ++p) {
        pBox.append( drawParamListItem(p) );
    }

}

function drawSelectedList() {

    let cBox = $('#choice-box');
    let count = 0;

    cBox.html('<div>Your Selection</div>');

    for(let p = 0; p < params.length; ++p) {
        if (params[p].selected) {
            cBox.append( drawSelecedListItem(p) );
            count++;
        }
    }

    if(count > 0) {
        $('#scroll-after-selection').addClass('scroll-indication');
    } else {
        $('#scroll-after-selection').removeClass('scroll-indication');
    }
}

function drawParamListItem(index) {

    if (index >= params.length) return;
    let itemString;

    if (params[index].active) {
        itemString = '<div class="param activated">' + params[index].name + '<button class="addParamButton" onclick="selectParam(' + index + ')">+</button><a class="paramSourceInfo" href="' + params[index].link + '" target="_blank">i</a></div>';
    } else {
        itemString = '<div class="param disabled">' + params[index].name + '<button class="addParamButton">+</button><a class="paramSourceInfo">i</a></div>';
    }
    
    return itemString;
}

function drawSelecedListItem(index) {

    if (index >= params.length) return;
    
    let itemString = '<div class="param activated">' + params[index].name + '<button class="addParamButton" onclick="deleteParam(' + index + ')">-</button><a class="paramSourceInfo" href="' + params[index].link + '" target="_blank">i</a></div>';
    
    return itemString;
}

function selectParam(index) {

    if (index >= params.length) return;

    params[index].selected = true;

    drawSelectedList();
}

function deleteParam(index) {
    if (index >= params.length) return;

    params[index].selected = false;

    drawSelectedList();
}

function showContactWin() {
    $('#contact-space').css('display', 'flex');
    captchaCode();
    $("input:text:visible:first").focus();
}

function hideContactWin() {
    $('#contact-space').hide();
}

function showImpressum() {
    $('#impressum-space').css('display', 'flex');
}

function hideImpressum() {
    $('#impressum-space').hide();
}

function captchaCode() {
    var Numb1, Numb2, Numb3, Numb4, Code;     
    Numb1 = (Math.ceil(Math.random() * 10)-1).toString();
    Numb2 = (Math.ceil(Math.random() * 10)-1).toString();
    Numb3 = (Math.ceil(Math.random() * 10)-1).toString();
    Numb4 = (Math.ceil(Math.random() * 10)-1).toString();
    Code = Numb1 + Numb2 + Numb3 + Numb4;
  $("#captcha span").remove();
  $("#captcha a").remove();
  $("#captcha").append("<span id='code'>" + Code + "</span><a class='reload-button' onclick='captchaCode();'><span></span></a>");
}

$('#contactForm').submit(function(){
    var captchaVal = $("#code").text();
    var captchaCode = $("#captcha-input").val();
    if (captchaVal == captchaCode) {
      $("#captcha-input").css({
        "color" : "#609D29"
      });
    }
    else {
      $("#captcha-input").css({
        "color" : "#CE3B46"
      });
      return;
    }
    
    var emailFilter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,10})+$/;   
    var emailText = $(".email").val();
    if (emailFilter.test(emailText)) {
      $(".email").css({
        "color" : "#609D29"
      });
    }
    else {
      $(".email").css({
        "color" : "#CE3B46"
      });
    }
    
    var nameFilter = /^([a-zA-Z \t]{3,15})+$/;
    var nameText = $(".name").val();
    if (nameFilter.test(nameText)) {
      $(".name").css({
        "color" : "#609D29"
      });
    }
    else {
      $(".name").css({
        "color" : "#CE3B46"
      });
    }
    
    var messageText = $(".message").text().length;
    if (messageText > 50) {
      $(".message").css({
        "color" : "#609D29"
      });
    }
    else {
      $(".message").css({
        "color" : "#CE3B46"
      });
    }
    
    if ((captchaVal !== captchaCode) || (!emailFilter.test(emailText)) || (!nameFilter.test(nameText)) || (messageText < 50)) {
      return false;
    }
    if ((captchaVal == captchaCode) && (emailFilter.test(emailText)) && (nameFilter.test(nameText)) && (messageText > 50)) {
      $("#contactForm").css("display", "none");
      $("#form").append("<h2>Message sent!</h2>");
      return false;
    }
  });       
/* }); */